<TABLE border="0" cellPadding="0" cellSpacing="0" width="100%">
  <TBODY>

  <TR><TD>
    <TABLE class="smallbox" cellPadding="0" cellSpacing="0">
      <TR class="header">
        <TD noWrap>Java/Servlet Specs</TD>
      </TR>
      <TR><TD width="*">
          <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
            <TBODY>
            <TR><TD height="5"></TD></TR>
            </TBODY>
          </TABLE>
      </TD></TR>     
      <TR class="body">
        <TD noWrap>

<A href="http://java.sun.com/j2se/1.4.2/docs/api/">J2SE 1.4.2 API Docs</A><BR>
<A href="http://java.sun.com/products/servlet/2.3/javadoc/index.html">Servlet 2.3 &amp; JSP 1.2 API Docs</A><BR>
<A href="http://www.jcp.org/aboutJava/communityprocess/first/jsr154/">(JSR-000154 Servlet 2.4 Spec)</A><BR>

        </TD>
      </TR>
    </TABLE>
  </TD></TR>

  <TR><TD>&nbsp;
  </TD></TR>

  <TR><TD>
    <TABLE class="smallbox" cellPadding="0" cellSpacing="0">
      <TR class="header">
        <TD noWrap>Servlet Forums</TD>
      </TR>
      <TR><TD width="*">
          <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
            <TBODY>
            <TR><TD height="5"></TD></TR>
            </TBODY>
          </TABLE>
      </TD></TR>     
      <TR class="body">
        <TD noWrap>

<A href="http://forums.java.sun.com/forum.jsp?forum=33">Java Servlet Technology Forum</A><BR> 
<A href="http://www.jguru.com/forums/Servlets">jGuru Servlets Forum</A><BR> 
<A href="http://www.servletforum.com/">ServletForum.com</A><BR> 

        </TD>
      </TR>
    </TABLE>
  </TD></TR>

  </TBODY>
</TABLE>

<%--
  Copyright (c) 2003, Bluecraft Software.  All rights reserved. 
  $Id: home.shortcuts.inc.jsp,v 1.4 2003/08/24 09:07:32 hyoon Exp $
--%>

