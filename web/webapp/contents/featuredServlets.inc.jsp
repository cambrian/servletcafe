<TABLE border="0" cellPadding="0" cellSpacing="0" width="100%">
  <TBODY>

  <TR><TD>
    <TABLE class="plainbox" cellPadding="0" cellSpacing="0">
      <TR class="header">
        <TD noWrap>Servlet of the Month</TD>
      </TR>
      <TR><TD width="*">
          <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
            <TBODY>
            <TR><TD height="5"></TD></TR>
            </TBODY>
          </TABLE>
      </TD></TR>     
      <TR>
        <TD>
    
<DL> 
<DT>&nbsp;&nbsp;<A href="http://authservlet.sourceforge.net/">AuthServlet&trade;</A></DT> 
<DD>
AuthServlet is a Java Servlet (and JSPs/TagLibs) which provides a simple pluggable authentication and role-based authorization service. It can be easily incorporated into any Servlet-based web applications.
</DD>
</DL> 

        </TD>
      </TR>
    </TABLE>
  </TD></TR>

  <TR><TD>&nbsp;
  </TD></TR>

  <TR><TD>
    <TABLE class="plainbox" cellPadding="0" cellSpacing="0">
      <TR class="header">
        <TD noWrap>Past Servlets of the Month</TD>
      </TR>
      <TR><TD width="*">
          <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
            <TBODY>
            <TR><TD height="5"></TD></TR>
            </TBODY>
          </TABLE>
      </TD></TR>     
      <TR>
        <TD>
    
<UL> 
<LI><A href="http://authservlet.sourceforge.net/">AuthServlet&trade;</A></LI> 
<LI><A href=""></A></LI> 
</UL> 

        </TD>
      </TR>
    </TABLE>
  </TD></TR>

  </TBODY>
</TABLE>

<%--
  Copyright (c) 2003, BlueCraft Software.  All rights reserved. 
  $Id: featuredServlets.inc.jsp,v 1.3 2003/01/28 07:02:03 hyoon Exp $
--%>

