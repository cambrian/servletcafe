<TABLE border="0" cellPadding="0" cellSpacing="0" width="100%">
  <TBODY>

  <TR><TD>
    <TABLE class="plainbox" cellPadding="0" cellSpacing="0">
      <TR class="header">
        <TD noWrap>Message Boards</TD>
      </TR>
      <TR><TD width="*">
          <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
            <TBODY>
            <TR><TD height="5"></TD></TR>
            </TBODY>
          </TABLE>
      </TD></TR>     
      <TR>
        <TD>
    
<UL> 
<LI><A href="http://forums.java.sun.com/forum.jsp?forum=33">The Java Servlet Technology Forum</A></LI> 
<LI><A href="http://www.servletforum.com/">ServletForum.com</A></LI> 
<LI><A href="http://www.jguru.com/forums/Servlets">Java Guru Servlets Forum</A></LI> 
<LI><A href="http://www.jguru.com/forums/JSP">Java Guru JSP Forum</A></LI> 
<LI><A href="http://sourceforge.net/forum/forum.php?forum_id=279425">WebStone Developers' Forum</A></LI> 
<LI><A href="http://sourceforge.net/forum/forum.php?forum_id=231902">AuthServlet Developers' Forum</A></LI> 
</UL> 

        </TD>
      </TR>
    </TABLE>
  </TD></TR>

  <TR><TD>&nbsp;
  </TD></TR>

  <TR><TD>
    <TABLE class="plainbox" cellPadding="0" cellSpacing="0">
      <TR class="header">
        <TD noWrap>Chat Rooms</TD>
      </TR>
      <TR><TD width="*">
          <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
            <TBODY>
            <TR><TD height="5"></TD></TR>
            </TBODY>
          </TABLE>
      </TD></TR>     
      <TR>
        <TD>
    
<UL> 
<LI><A href=""></A></LI> 
</UL> 

        </TD>
      </TR>
    </TABLE>
  </TD></TR>

  </TBODY>
</TABLE>

<%--
  Copyright (c) 2003, BlueCraft Software.  All rights reserved. 
  $Id: messageBoards.inc.jsp,v 1.5 2003/08/24 09:07:32 hyoon Exp $
--%>

