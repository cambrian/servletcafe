<TABLE cellspacing="2" cellpadding="2" border="0" width="100%" align="center" style="background: #f5faff;">
<TR width="100%">
<TD width="65%" align="left" vAlign="top">

<H3><SPAN style="color:gray;">&lt;%</SPAN> Servlet&nbsp;News <SPAN style="color:gray;">%&gt;</SPAN></H3>

<P>
<DL>
<DT>New Servlet Specifications</DT>
<DD><A href="http://www.jcp.org/aboutJava/communityprocess/first/jsr154/">JSR-000154 Servlet 2.4 Spec</A></DD>
<DT>Dec. 19, 2002</DT>
<DD><A href="http://jakarta.apache.org/site/news.html#1219.1">Security update: Tomcat 4.1.18 Stable Released</A></DD>
<DT>Dec. 06, 2002</DT>
<DD><A href="http://xml.apache.org/news.html#N10018">Cocoon 2.0.4 released</A></DD>
</DL>
</P>


</TD>

<TD width="2%">
&nbsp;
</TD>

<TD width="33%" align="left" vAlign="top" noWrap>
<%@ include file="servletResources.shortcuts.inc.jsp" %>
</TD>
</TR>

<TR>
  <TD colSpan="3"><IMG height="6" alt="" src="images/blank.gif" width="1" border="0"></TD>
</TR>

<TR width="100%">
<TD colSpan="3" width="100%" align="left" vAlign="top">

<H3><SPAN style="color:gray;">&lt;%</SPAN> Selected Articles from Various Sources <SPAN style="color:gray;">%&gt;</SPAN></H3>

<P>
<UL>
<LI><A href="http://www.onjava.com/pub/a/onjava/excerpt/jebp_3/index3.html">Servlet Best Practices, Part 3</A></LI>
<LI><A href="http://www.onjava.com/pub/a/onjava/excerpt/jebp_3/index2.html">Servlet Best Practices, Part 2</A></LI>
<LI><A href="http://www.onjava.com/pub/a/onjava/excerpt/jebp_3/index1.html">Servlet Best Practices, Part 1</A></LI>
<LI><A href="http://dev2dev.bea.com/articlesnews/discussion/thread.jsp?thread=Buzzard">Enabling Enterprise portal Integration with Web Services</A></LI>
<LI><A href="http://www.javaworld.com/javaworld/javatips/jw-javatip133.html?">Java Tip 133: More on typesafe enums</A></LI>
</UL>
</P>


</TD>
</TR>
</TABLE>

<%--
  Copyright (c) 2003, BlueCraft Software.  All rights reserved. 
  $Id: servletResources.inc.jsp,v 1.3 2003/01/27 00:28:01 hyoon Exp $
--%>

