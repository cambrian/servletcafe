<TABLE border="0" cellPadding="0" cellSpacing="0" width="100%">
  <TBODY>

  <TR><TD>
    <TABLE class="plainbox" cellPadding="0" cellSpacing="0">
      <TR class="header">
        <TD noWrap>Java Platforms</TD>
      </TR>
      <TR><TD width="*">
          <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
            <TBODY>
            <TR><TD height="5"></TD></TR>
            </TBODY>
          </TABLE>
      </TD></TR>     
      <TR>
        <TD>
    
<UL> 
<LI><A href="http://java.sun.com/j2se/">Java&trade; 2 Platform, Standard Edition (J2SE&trade;)</A></LI> 
<LI><A href="http://java.sun.com/j2se/1.4.2/docs/api/">J2SE 1.4.2 API Specification</A></LI> 
<LI><A href="http://java.sun.com/j2se/1.3/docs/api/">J2SE 1.3.1 API Specification</A></LI> 
<LI><A href="http://java.sun.com/products/jdk/1.2/docs/api/">J2SE 1.2.2 API Specification</A></LI> 
<LI><A href="http://java.sun.com/j2ee/">Java&trade; 2 Platform, Enterprise Edition (J2EE&trade;)</A></LI> 
<LI><A href="http://java.sun.com/j2ee/1.4/docs/api">J2EE 1.4 Beta API Specification</A></LI> 
<LI><A href="http://java.sun.com/j2ee/sdk_1.3/techdocs/api/">J2EE 1.3.1 API Specification</A></LI> 
<LI><A href="http://java.sun.com/j2ee/sdk_1.2.1/techdocs/api/">J2EE 1.2.1 API Specification</A></LI> 
<!--
<LI><A href="http://java.sun.com/j2me/">Java&trade; 2 Platform, Micro Edition (J2ME&trade;)</A></LI> 
-->
</UL> 

        </TD>
      </TR>
    </TABLE>
  </TD></TR>

  <TR><TD>&nbsp;
  </TD></TR>

  <TR><TD>
    <TABLE class="plainbox" cellPadding="0" cellSpacing="0">
      <TR class="header">
        <TD noWrap>Servlet/JSP Specifcations</TD>
      </TR>
      <TR><TD width="*">
          <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
            <TBODY>
            <TR><TD height="5"></TD></TR>
            </TBODY>
          </TABLE>
      </TD></TR>     
      <TR>
        <TD>
    
<UL> 
<LI><A href="http://java.sun.com/products/servlet/">Java&trade; Servlet Technology</A></LI> 
<LI><A href="http://java.sun.com/products/jsp/">JavaServer Pages&trade;</A></LI> 
<LI><A href="http://java.sun.com/j2ee/javaserverfaces/">JavaServer&trade; Faces</A></LI> 
<LI><A href="http://java.sun.com/products/jsp/jstl/">JavaServer Pages&trade; Standard Tag Library</A></LI> 
<LI><A href="http://www.jcp.org/aboutJava/communityprocess/first/jsr154/">JSR-000154 Java&trade; Servlet 2.4 Specification</A></LI> 
<LI><A href="http://www.jcp.org/aboutJava/communityprocess/first/jsr152/">JSR-000152 JavaServer Pages&trade; 2.0 Specification </A></LI> 
<LI><A href="http://www.jcp.org/aboutJava/communityprocess/final/jsr053/">JSR-000053 Java&trade; Servlet 2.3 and JavaServer Pages&trade; 1.2 Specifications</A></LI>
<LI><A href="http://java.sun.com/products/servlet/2.3/javadoc/index.html">Java&trade; Servlet 2.3 and JavaServer Pages&trade; 1.2 API Specifications</A></LI> 
<LI><A href="http://java.sun.com/products/servlet/2.2/javadoc/index.html">Java&trade; Servlet 2.2 and JavaServer Pages&trade; 1.1 API Specifications</A></LI> 
<LI><A href="http://www.jcp.org/en/jsr/detail?id=168">JSR-000168 Portlet Specification</A></LI> 
</UL> 

        </TD>
      </TR>
    </TABLE>
  </TD></TR>

  <TR><TD>&nbsp;
  </TD></TR>

  <TR><TD>
    <TABLE class="plainbox" cellPadding="0" cellSpacing="0">
      <TR class="header">
        <TD noWrap>Java Technology &amp; XML</TD>
      </TR>
      <TR><TD width="*">
          <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
            <TBODY>
            <TR><TD height="5"></TD></TR>
            </TBODY>
          </TABLE>
      </TD></TR>     
      <TR>
        <TD>
    
<UL> 
<LI><A href="http://java.sun.com/xml/">Java&trade; Technology and XML</A></LI> 
<LI><A href="http://java.sun.com/webservices/">Java&trade; Technology and Web Services</A></LI> 
<LI><A href="http://java.sun.com/xml/downloads/jaxb.html">JAXB Specification v0.75, Public Draft 2</A></LI> 
<LI><A href="http://java.sun.com/xml/downloads/jaxm.html">JAXM Specification v1.1</A></LI> 
<LI><A href="http://java.sun.com/xml/downloads/jaxp.html">JAXP Specification v1.1</A></LI> 
<LI><A href="http://java.sun.com/xml/downloads/jaxr.html">JAXR Specification v1.0</A></LI> 
<LI><A href="http://java.sun.com/xml/downloads/jaxrpc.html">JAX-RPC Specification v1.0</A></LI> 
<LI><A href="http://java.sun.com/xml/downloads/saaj.html">SAAJ Specification v1.1</A></LI> 
</UL> 

        </TD>
      </TR>
    </TABLE>
  </TD></TR>

  <TR><TD>&nbsp;
  </TD></TR>

  <TR><TD>
    <TABLE class="plainbox" cellPadding="0" cellSpacing="0">
      <TR class="header">
        <TD noWrap>Miscellaneous Standards</TD>
      </TR>
      <TR><TD width="*">
          <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
            <TBODY>
            <TR><TD height="5"></TD></TR>
            </TBODY>
          </TABLE>
      </TD></TR>     
      <TR>
        <TD>
    
<UL> 
<LI><A href="http://java.sun.com/products/javabeans/">JavaBeans&trade; Technology</A></LI> 
<LI><A href="http://java.sun.com/products/javahelp/">Java Help&trade; Technology</A></LI> 
<LI><A href="http://java.sun.com/products/javamail/">JavaMail&trade;</A></LI> 
<LI><A href="http://java.sun.com/products/jms/">Java Message Service (JMS)</A></LI> 
<LI><A href="http://java.sun.com/products/jdbc/">JDBC&trade; Technology</A></LI> 
<LI><A href="http://java.sun.com/products/jdo/">Java Data Objects (JDO)</A></LI> 
<LI><A href="http://java.sun.com/products/jaas/">Java&trade; Authentication and Authorization Service (JAAS)</A></LI> 
<LI><A href="http://java.sun.com/security/jaas/doc/pam.html">Pluggable Authentication Module (PAM) framework</A></LI> 
<LI><A href="http://java.sun.com/products/jsse/">Java Secure Socket Extension (JSSE)</A></LI> 
</UL> 

        </TD>
      </TR>
    </TABLE>
  </TD></TR>

  </TBODY>
</TABLE>

<%--
  Copyright (c) 2003, Bluecraft Software.  All rights reserved. 
  $Id: servletSpecs.inc.jsp,v 1.6 2003/08/24 09:07:32 hyoon Exp $
--%>

