<TABLE border="0" cellPadding="0" cellSpacing="0" width="100%">
  <TBODY>

  <TR><TD>
    <TABLE class="plainbox" cellPadding="0" cellSpacing="0">
      <TR class="header">
        <TD noWrap>Framework Servlets</TD>
      </TR>
      <TR><TD width="*">
          <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
            <TBODY>
            <TR><TD height="5"></TD></TR>
            </TBODY>
          </TABLE>
      </TD></TR>     
      <TR>
        <TD>
    
<UL> 
<LI><A href="http://webstone.sourceforge.net/">WebStone&trade; Web Application Framework</A></LI> 
<LI><A href="http://jakarta.apache.org/struts/">Jakarta Struts</A></LI> 
<LI><A href="http://jakarta.apache.org/jetspeed/">Jakarta JetSpeed</A></LI> 
<LI><A href="http://jakarta.apache.org/turbine/">Jakarta Turbine</A></LI> 
<LI><A href="http://www.jpublish.org/">JPublish Open Source Web Publishing</A></LI> 
</UL> 

        </TD>
      </TR>
    </TABLE>
  </TD></TR>

  <TR><TD>&nbsp;
  </TD></TR>

  <TR><TD>
    <TABLE class="plainbox" cellPadding="0" cellSpacing="0">
      <TR class="header">
        <TD noWrap>Application Servlets</TD>
      </TR>
      <TR><TD width="*">
          <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
            <TBODY>
            <TR><TD height="5"></TD></TR>
            </TBODY>
          </TABLE>
      </TD></TR>     
      <TR>
        <TD>
    
<UL> 
<LI><A href="http://authservlet.sourceforge.net/">AuthServlet&trade;</A></LI> 
<LI><A href="http://www.jspinsider.com/beans/beans/email/BeanMailer/">BeanMailer</A></LI> 
<LI><A href="http://www.javasource.org/GifServlet/">GifServlet</A></LI> 
<LI><A href="http://www.coolservlets.com/CSGuestbook/">CSGuestbook</A></LI> 
<LI><A href="http://www.servletsuite.com/servlets/webmail.htm">WebMail servlet</A></LI> 
<LI><A href="http://coldjava.hypermart.net/servlets/mboard.htm">MessageBoard servlet 2.43</A></LI> 
<LI><A href="http://www.servlets.com/cos/index.html">com.oreilly.servlet</A></LI> 
<LI><A href="http://www.nebulaos.com/osdirectory/index.html">Nebula OSDirectory</A></LI> 
</UL> 

        </TD>
      </TR>
    </TABLE>
  </TD></TR>

  </TBODY>
</TABLE>

<%--
  Copyright (c) 2003, BlueCraft Software.  All rights reserved. 
  $Id: assortedServlets.inc.jsp,v 1.6 2003/08/24 09:07:32 hyoon Exp $
--%>

