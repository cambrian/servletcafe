<TABLE cellspacing="2" cellpadding="2" border="0" width="100%" align="center" style="background: #f5faff;">
<TR width="100%">
<TD width="65%" align="left" vAlign="top">

<H3><SPAN style="color:gray;">&lt;%</SPAN> Servlet&nbsp;List <SPAN style="color:gray;">%&gt;</SPAN></H3>

<P>
Some of the reusable servlets are compiled on this section. Many of them are open-source and/or freely available. Please follow the links from the left-hand side navigation menu.
</P>

</TD>

<TD width="2%">
&nbsp;
</TD>

<TD width="33%" align="left" vAlign="top" noWrap>
<%@ include file="servletList.shortcuts.inc.jsp" %>
</TD>
</TR>

<TR>
  <TD colSpan="3"><IMG height="6" alt="" src="images/blank.gif" width="1" border="0"></TD>
</TR>

<TR width="100%">
<TD colSpan="3" width="100%" align="left" vAlign="top">

<H3><SPAN style="color:gray;">&lt;%</SPAN> Servlet Modules <SPAN style="color:gray;">%&gt;</SPAN></H3>

<P>
<em>Servlet Module</em> is still an evolving concept. The exact definition is currently being worked out, and it will be posted on this site as soon as it becomes available.
</P>



</TD>
</TR>
</TABLE>

<%--
  Copyright (c) 2003, BlueCraft Software.  All rights reserved. 
  $Id: servletList.inc.jsp,v 1.4 2003/01/28 07:02:03 hyoon Exp $
--%>

