<TABLE border="0" cellPadding="0" cellSpacing="0" width="100%">
  <TBODY>

  <TR><TD>
    <TABLE class="plainbox" cellPadding="0" cellSpacing="0">
      <TR class="header">
        <TD noWrap>Servlet Cafe&trade; Mailing Lists</TD>
      </TR>
      <TR><TD width="*">
          <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
            <TBODY>
            <TR><TD height="5"></TD></TR>
            </TBODY>
          </TABLE>
      </TD></TR>     
      <TR>
        <TD>
    
<UL> 
<LI><A href=""></A></LI> 
</UL> 

        </TD>
      </TR>
    </TABLE>
  </TD></TR>

  <TR><TD>&nbsp;
  </TD></TR>

  <TR><TD>
    <TABLE class="plainbox" cellPadding="0" cellSpacing="0">
      <TR class="header">
        <TD noWrap>Miscellaneous Mailing Lists</TD>
      </TR>
      <TR><TD width="*">
          <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
            <TBODY>
            <TR><TD height="5"></TD></TR>
            </TBODY>
          </TABLE>
      </TD></TR>     
      <TR>
        <TD>
    
<UL> 
<LI><A href="http://lists.sourceforge.net/lists/listinfo/webstone-announce">WebStone Announcement</A></LI> 
<LI><A href="http://lists.sourceforge.net/lists/listinfo/webstone-devel">WebStone Developers' List</A></LI> 
<LI><A href="http://lists.sourceforge.net/lists/listinfo/authservlet-announce">AuthServlet Announcement</A></LI> 
<LI><A href="http://lists.sourceforge.net/lists/listinfo/authservlet-devel">AuthServlet Developers' List</A></LI> 
<LI><A href="http://jakarta.apache.org/site/mail2.html">The Jakarta Announcement List</A></LI> 
<LI><A href="http://www.mail-archive.com/struts-user@jakarta.apache.org/">The Struts User List</A></LI> 
<LI><A href="http://www.mail-archive.com/tomcat-user@jakarta.apache.org/">The Tomcat User List</A></LI> 
</UL> 

        </TD>
      </TR>
    </TABLE>
  </TD></TR>

  </TBODY>
</TABLE>

<%--
  Copyright (c) 2003, BlueCraft Software.  All rights reserved. 
  $Id: mailingLists.inc.jsp,v 1.3 2003/08/24 09:07:32 hyoon Exp $
--%>

