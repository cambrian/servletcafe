<TABLE border="0" cellPadding="0" cellSpacing="0" width="100%">
  <TBODY>

  <TR><TD>
    <TABLE class="plainbox" cellPadding="0" cellSpacing="0">
      <TR class="header">
        <TD noWrap>About Servlet Cafe&trade;</TD>
      </TR>
      <TR><TD width="*">
          <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
            <TBODY>
            <TR><TD height="5"></TD></TR>
            </TBODY>
          </TABLE>
      </TD></TR>     
      <TR>
        <TD>

<DIV> 
Servlet Cafe&trade; is maintained by <A href="http://www.bluecraft.com/" class="cammoflage">BlueCraft Software</A>.
</DIV> 

<DIV> 
<TABLE border="0" cellPadding="0" cellSpacing="0" width="100%">
  <TBODY>
  <TR>
  <TD width="120">Created:</TD>
  <TD>Jan. 26th, 2003</TD>
  </TR>
  <TR>
  <TD width="120">Last-Updated:</TD>
  <TD>Jan. 26th, 2003</TD>
  </TR>
  </TBODY>
</TABLE>
</DIV> 

<DIV> 
Copyright &copy; 2003, <A href="http://www.bluecraft.com/" class="cammoflage">BlueCraft Software</A>. All rights reserved.
</DIV> 

        </TD>
      </TR>
    </TABLE>
  </TD></TR>

  <TR><TD>&nbsp;
  </TD></TR>

  <TR><TD>
    <TABLE class="plainbox" cellPadding="0" cellSpacing="0">
      <TR class="header">
        <TD noWrap>About BlueCraft Software</TD>
      </TR>
      <TR><TD width="*">
          <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
            <TBODY>
            <TR><TD height="5"></TD></TR>
            </TBODY>
          </TABLE>
      </TD></TR>     
      <TR>
        <TD>

<DIV> 
<A href="http://www.bluecraft.com/" class="cammoflage">BlueCraft Software</A> provides <em>Computing Solutions on Demand&trade;</em>. It specializes in Internet Technologies and User Interface Softwares on various platforms.
</DIV> 

<DIV> 
<TABLE border="0" cellPadding="0" cellSpacing="0" width="100%">
  <TBODY>
  <TR>
  <TD width="120">Request Info:</TD>
  <TD><A href="mailto:info&#64;bluecraft.com" class="cammoflage">info&#64;bluecraft.com</A></TD>
  </TR>
  <TR>
  <TD width="120">Contact Sales:</TD>
  <TD><A href="mailto:sales&#64;bluecraft.com" class="cammoflage">sales&#64;bluecraft.com</A></TD>
  </TR>
  </TBODY>
</TABLE>
</DIV> 

        </TD>
      </TR>
    </TABLE>
  </TD></TR>

  </TBODY>
</TABLE>

<%--
  Copyright (c) 2003, BlueCraft Software.  All rights reserved. 
  $Id: about.inc.jsp,v 1.4 2003/08/24 09:07:32 hyoon Exp $
--%>

