<TABLE cellspacing="2" cellpadding="2" border="0" width="100%" align="center" style="background: #f5faff;">
<TR width="100%">
<TD width="65%" align="left" vAlign="top">

<H3><SPAN style="color:gray;">&lt;%</SPAN> Servlet&nbsp;Cafe <SPAN style="color:gray;">%&gt;</SPAN></H3>

<P>
Servlet Cafe&trade; is a web site dedicated to promote development and deployment of <b>Servlet Modules</b>.
</P>

<P>
This site is under heavy development at the moment. Please check back often.
</P>


</TD>

<TD width="2%">
&nbsp;
</TD>

<TD width="33%" align="left" vAlign="top" noWrap>
<%@ include file="home.shortcuts.inc.jsp" %>
</TD>
</TR>

<TR>
  <TD colSpan="3"><IMG height="6" alt="" src="images/blank.gif" width="1" border="0"></TD>
</TR>

<TR width="100%">
<TD colSpan="3" width="100%" align="left" vAlign="top">

<H3><SPAN style="color:gray;">&lt;%</SPAN> Servlet Modules <SPAN style="color:gray;">%&gt;</SPAN></H3>

<P>
What is a <b>Servlet Module</b>?
</P>

<P>
WebApp (on Java Platform) typically consists of Servlets, JSP pages, tag libraries, and supporting Java classes such as beans as well as various static resources such as HTML pages and image files. All these elements are interconnected or related with each other, and as such WebApp is generally considered a minimal unit of which web sites are made. However, even a simple WebApp can be divided into many (mutually-dependent) functional units. .....

Even though many of these units are logically or functionally independent of each other, they are tightly integrated due to the physical constraints. The integration points are
<UL>
<LI>Sharing of directory structures</LI>
<LI>Common use of the WebApp deployment descriptor file, web.xml</LI>
<LI>Sharing of some common resources, such as style files</LI>
</UL>
</P>

<P>
<b>Servlet Module</b> is a sub-unit of WebApp that can be easily integrated into WebApps in a <em>Plug and Play</em> fasion.
</P>



</TD>
</TR>
</TABLE>

<%--
  Copyright (c) 2003, Bluecraft Software.  All rights reserved. 
  $Id: home.inc.jsp,v 1.9 2003/08/24 09:07:32 hyoon Exp $
--%>

