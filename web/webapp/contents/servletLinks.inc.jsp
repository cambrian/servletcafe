<TABLE border="0" cellPadding="0" cellSpacing="0" width="100%">
  <TBODY>

  <TR><TD>
    <TABLE class="plainbox" cellPadding="0" cellSpacing="0">
      <TR class="header">
        <TD noWrap>Java Portals</TD>
      </TR>
      <TR><TD width="*">
          <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
            <TBODY>
            <TR><TD height="5"></TD></TR>
            </TBODY>
          </TABLE>
      </TD></TR>     
      <TR>
        <TD>
    
<UL> 
<LI><A href="http://www.javaworld.net/">Java World</A></LI> 
<LI><A href="http://www.jguru.com/">Java Guru</A></LI> 
<LI><A href="http://www.onjava.com/">O'Reilly ONJava.com</A></LI> 
<LI><A href="http://www.javalobby.org/">JavaLobby</A></LI> 
</UL> 

        </TD>
      </TR>
    </TABLE>
  </TD></TR>

  <TR><TD>&nbsp;
  </TD></TR>

  <TR><TD>
    <TABLE class="plainbox" cellPadding="0" cellSpacing="0">
      <TR class="header">
        <TD noWrap>Web Sites Devoted to Servlets</TD>
      </TR>
      <TR><TD width="*">
          <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
            <TBODY>
            <TR><TD height="5"></TD></TR>
            </TBODY>
          </TABLE>
      </TD></TR>     
      <TR>
        <TD>
    
<UL> 
<LI><A href="http://www.servlets.com/">Servlets.com</A></LI> 
<LI><A href="http://www.coolservlets.com/">CoolServlets.com</A></LI> 
<LI><A href="http://www.javasource.org/">JavaSource.org</A></LI> 
<LI><A href="http://www.jspin.com/">JSP Resource Index</A></LI> 
<LI><A href="http://www.jspinsider.com/">JSP Insider</A></LI> 
<LI><A href="http://jsptags.com/">&lt;jsptags.com&gt;</A></LI> 
<LI><A href="http://www.servletsource.com/">ServletSource.com</A></LI> 
<LI><A href="http://www.coreservlets.com/">Core Servlets and JavaServer Pages (JSP)</A></LI> 
<LI><A href="http://www.jaagle.com/">Jaggle! Java Repository</A></LI> 
</UL> 

        </TD>
      </TR>
    </TABLE>
  </TD></TR>

  <TR><TD>&nbsp;
  </TD></TR>

  <TR><TD>
    <TABLE class="plainbox" cellPadding="0" cellSpacing="0">
      <TR class="header">
        <TD noWrap>Search the Web</TD>
      </TR>
      <TR><TD width="*">
          <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
            <TBODY>
            <TR><TD height="5"></TD></TR>
            </TBODY>
          </TABLE>
      </TD></TR>     
      <TR>
        <TD>


<BR>
<!-- Begin Yahoo Search Form -->
<form method="GET" action="http://search.yahoo.com/search">
   <img src="http://us.i1.yimg.com/us.yimg.com/i/yahootogo/ytg_search.gif" width=98 height=23 align=top alt="[ Yahoo! ]">
   <input type="text" name="p" value="" size=18>
   <input type="submit" name="name">
   <font size=1>
   <a href="http://search.yahoo.com/search/options">options</a>
   </font>
</form>
<!-- End Yahoo Search Form -->


        </TD>
      </TR>
    </TABLE>
  </TD></TR>

  </TBODY>
</TABLE>

<%--
  Copyright (c) 2003, Bluecraft Software.  All rights reserved. 
  $Id: servletLinks.inc.jsp,v 1.6 2003/08/24 09:07:32 hyoon Exp $
--%>

