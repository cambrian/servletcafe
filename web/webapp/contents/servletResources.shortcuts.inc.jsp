<TABLE border="0" cellPadding="0" cellSpacing="0" width="100%">
  <TBODY>

  <TR><TD>
    <TABLE class="smallbox" cellPadding="0" cellSpacing="0">
      <TR class="header">
        <TD noWrap>Java/Servlet Specs</TD>
      </TR>
      <TR><TD width="*">
          <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
            <TBODY>
            <TR><TD height="5"></TD></TR>
            </TBODY>
          </TABLE>
      </TD></TR>     
      <TR class="body">
        <TD noWrap>

<A href="http://java.sun.com/j2se/1.4.1/docs/api/">J2SE 1.4.1 API Spec</A><BR>
<A href="http://java.sun.com/products/servlet/2.3/javadoc/index.html">Servlet 2.3 &amp; JSP 1.2 API Spec</A><BR>
<A href="http://www.jcp.org/aboutJava/communityprocess/first/jsr154/">(JSR-000154 Servlet 2.4 Spec)</A><BR>

        </TD>
      </TR>
    </TABLE>
  </TD></TR>

  <TR><TD>&nbsp;
  </TD></TR>

  <TR><TD>
    <TABLE class="smallbox" cellPadding="0" cellSpacing="0">
      <TR class="header">
        <TD noWrap>Internet/Web Standards</TD>
      </TR>
      <TR><TD width="*">
          <TABLE border=0 cellPadding=0 cellSpacing=0 width="100%">
            <TBODY>
            <TR><TD height="5"></TD></TR>
            </TBODY>
          </TABLE>
      </TD></TR>     
      <TR class="body">
        <TD noWrap>

<A href="http://www.ietf.org/rfc.html">IETF RFCs</A><BR> 
<A href="http://www.w3.org/TR/">W3C Recommendations</A><BR> 

        </TD>
      </TR>
    </TABLE>
  </TD></TR>

  </TBODY>
</TABLE>

<%--
  Copyright (c) 2003, BlueCraft Software.  All rights reserved. 
  $Id: servletResources.shortcuts.inc.jsp,v 1.3 2003/01/27 00:28:01 hyoon Exp $
--%>

