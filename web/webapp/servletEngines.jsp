<%@ page language="java" %>
<%@ taglib uri='/tags/struts-template' prefix='template' %>

<template:insert template='templates/mainTemplate.jsp'>
  <template:put name='title' content='Servlet Cafe | Servlet Resources | Servlet Engines' direct='true'/>
  <template:put name='heading' content='Servlet Engines &amp; App Servers' direct='true'/>
  <template:put name='header' content='/common/header.inc.jsp' />
  <template:put name='sidebar' content='/common/sidebar.inc.jsp' />
  <template:put name='content' content='/contents/servletEngines.inc.jsp'/>
  <template:put name='footer' content='/common/footer.inc.jsp' />
</template:insert>


<!--
  Copyright (c) 2003, BlueCraft Software.  All rights reserved. 
  Requested URI: <%= request.getRequestURI() %>
  $Id: servletEngines.jsp,v 1.1 2003/01/08 09:10:52 hyoon Exp $
-->
