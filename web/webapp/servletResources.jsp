<%@ page language="java" %>
<%@ taglib uri='/tags/struts-template' prefix='template' %>

<template:insert template='templates/mainTemplate.jsp'>
  <template:put name='title' content='Servlet Cafe | Servlet Resources' direct='true'/>
  <template:put name='heading' content='Servlet Resources on the Web' direct='true'/>
  <template:put name='header' content='/common/header.inc.jsp' />
  <template:put name='sidebar' content='/common/sidebar.inc.jsp' />
  <template:put name='content' content='/contents/servletResources.inc.jsp'/>
  <template:put name='footer' content='/common/footer.inc.jsp' />
</template:insert>


<!--
  Copyright (c) 2003, BlueCraft Software.  All rights reserved. 
  Requested URI: <%= request.getRequestURI() %>
  $Id: servletResources.jsp,v 1.3 2003/01/10 07:43:35 hyoon Exp $
-->
