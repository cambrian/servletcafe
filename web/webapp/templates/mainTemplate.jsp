<%@ page language="java" %>
<%@ taglib uri='/tags/struts-template' prefix='template' %>

<HTML>
<HEAD>
<META http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<META name="robot" CONTENT="all">
<META name="summary" content="Java Servlet and JSP Resources">
<META name="keywords" content="Java, Servlet, JSP, Struts">
<META http-equiv="Page-Enter" content="blendTrans(Duration=1.0)">
<LINK rel="stylesheet" href="<%=request.getContextPath()%>/styles/servletcafe.css"
      charset="ISO-8859-1" type="text/css">
<TITLE><template:get name='title'/></TITLE>
</HEAD>
<BODY class="mainTemplate">

<table width="100%">
   <tr valign='top' align="left">
      <td noWrap><template:get name='sidebar'/></td>
      <td align="left" width="100%">
          <table width="100%">
            <tr width="100%"><td width="100%"><template:get name='header'/></td></tr>
            <tr><td><template:get name='content'/></td></tr>
            <tr><td><template:get name='footer'/></td></tr>
          </table>
      </td>
   </tr> 
</table>
</BODY>
</HTML>

<%--
  Copyright (c) 2003, BlueCraft Software.  All rights reserved. 
  $Id: mainTemplate.jsp,v 1.3 2003/01/26 22:17:11 hyoon Exp $
--%>
