<%@ page language="java" %>
<%@ taglib uri='/tags/struts-template' prefix='template' %>

<template:insert template='templates/mainTemplate.jsp'>
  <template:put name='title' content='Servlet Cafe | Servlet Resources | Servlet Specs' direct='true'/>
  <template:put name='heading' content='Java/Servlet Specifications' direct='true'/>
  <template:put name='header' content='/common/header.inc.jsp' />
  <template:put name='sidebar' content='/common/sidebar.inc.jsp' />
  <template:put name='content' content='/contents/servletSpecs.inc.jsp'/>
  <template:put name='footer' content='/common/footer.inc.jsp' />
</template:insert>


<!--
  Copyright (c) 2003, Bluecraft Software.  All rights reserved. 
  Requested URI: <%= request.getRequestURI() %>
  $Id: servletSpecs.jsp,v 1.2 2003/08/24 09:07:32 hyoon Exp $
-->
