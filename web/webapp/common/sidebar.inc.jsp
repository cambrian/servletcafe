<%@ page language="java" %>
<%@ taglib uri='/tags/struts-template' prefix='template' %>
<table width='145'>
<tr><td colSpan="2">&nbsp;</td></tr>
<tr><td colSpan="2" vAlign="top"><A href="<%=request.getContextPath()%>/"><SPAN class="menuTitle">Servlet&nbsp;Cafe</SPAN></A></td></tr>
<tr vAlign="top">
  <td width="2" vAlign="top" align="left">&nbsp;</td>
  <td vAlign="top" align="left" width="143"><a href="<%=request.getContextPath()%>/about.jsp"><SPAN class="menuItem">About</SPAN></a></td>
</tr>
</table>

<table width='145'>
<tr><td colSpan="2">&nbsp;</td></tr>
<tr><td colSpan="2" vAlign="top"><A href="<%=request.getContextPath()%>/servletList.jsp"><SPAN class="menuTitle">Servlet&nbsp;List</SPAN></A></td></tr>
<tr vAlign="top">
  <td width="2" vAlign="top" align="left">&nbsp;</td>
  <td vAlign="top" align="left" width="143"><a href="<%=request.getContextPath()%>/featuredServlets.jsp"><SPAN class="menuItem">Featured&nbsp;Servlets</SPAN></a></td>
</tr>
<tr vAlign="top">
  <td width="2" vAlign="top" align="left">&nbsp;</td>
  <td vAlign="top" align="left" width="143"><a href="<%=request.getContextPath()%>/assortedServlets.jsp"><SPAN class="menuItem">Assorted&nbsp;Servlets</SPAN></a></td>
</tr>
</table>

<table width='145'>
<tr><td colSpan="2">&nbsp;</td></tr>
<tr><td colSpan="2" vAlign="top"><A href="<%=request.getContextPath()%>/servletResources.jsp"><SPAN class="menuTitle">Servlet&nbsp;Resources</SPAN></A></td></tr>
<tr vAlign="top">
  <td width="2" vAlign="top" align="left">&nbsp;</td>
  <td vAlign="top" align="left" width="143"><a href="<%=request.getContextPath()%>/servletSpecs.jsp"><SPAN class="menuItem">Servlet&nbsp;Specs</SPAN></a></td>
</tr>
<tr vAlign="top">
  <td width="2" vAlign="top" align="left">&nbsp;</td>
  <td vAlign="top" align="left" width="143"><a href="<%=request.getContextPath()%>/servletEngines.jsp"><SPAN class="menuItem">Servlet&nbsp;Engines</SPAN></a></td>
</tr>
<tr vAlign="top">
  <td width="2" vAlign="top" align="left">&nbsp;</td>
  <td vAlign="top" align="left" width="143"><a href="<%=request.getContextPath()%>/servletTools.jsp"><SPAN class="menuItem">Servlet&nbsp;Tools</SPAN></a></td>
</tr>
<tr vAlign="top">
  <td width="2" vAlign="top" align="left">&nbsp;</td>
  <td vAlign="top" align="left" width="143"><a href="<%=request.getContextPath()%>/servletISPs.jsp"><SPAN class="menuItem">Servlet&nbsp;ISPs</SPAN></a></td>
</tr>
<tr vAlign="top">
  <td width="2" vAlign="top" align="left">&nbsp;</td>
  <td vAlign="top" align="left" width="143"><a href="<%=request.getContextPath()%>/servletLinks.jsp"><SPAN class="menuItem">Servlet&nbsp;Links</SPAN></a></td>
</tr>
</table>

<table width='145'>
<tr><td colSpan="2">&nbsp;</td></tr>
<tr><td colSpan="2" vAlign="top"><A href="<%=request.getContextPath()%>/servletForums.jsp"><SPAN class="menuTitle">Servlet&nbsp;Forums</SPAN></A></td></tr>
<tr vAlign="top">
  <td width="2" vAlign="top" align="left">&nbsp;</td>
  <td vAlign="top" align="left" width="143"><a href="<%=request.getContextPath()%>/messageBoards.jsp"><SPAN class="menuItem">Message&nbsp;Boards</SPAN></a></td>
</tr>
<tr vAlign="top">
  <td width="2" vAlign="top" align="left">&nbsp;</td>
  <td vAlign="top" align="left" width="143"><a href="<%=request.getContextPath()%>/mailingLists.jsp""><SPAN class="menuItem">Mailing&nbsp;Lists</SPAN></a></td>
</tr>
</table>


<table width='145'>
<tr><td colSpan="2">&nbsp;</td></tr>
<tr><td colSpan="2">&nbsp;</td></tr>
<tr vAlign="bottom">
  <td width="5" valign="top" >&nbsp;</td>
  <td valign="top"><a href="http://jakarta.apache.org/struts/" title="Powered by Strtuts"><img src="<%=request.getContextPath()%>/images/struts-power.gif" alt="Powered by Struts"></a></td>
</tr>
</table>


<%--
  Copyright (c) 2003, BlueCraft Software.  All rights reserved. 
  $Id: sidebar.inc.jsp,v 1.4 2003/01/09 10:09:17 hyoon Exp $
--%>
